package univ.ibam.gestionfichier;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestionfichierApplication {
    public static void main(String[] args) {
        SpringApplication.run(GestionfichierApplication.class, args);
    }

}
