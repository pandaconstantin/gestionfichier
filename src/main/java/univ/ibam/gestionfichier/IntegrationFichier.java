package univ.ibam.gestionfichier;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.integration.annotation.InboundChannelAdapter;
import org.springframework.integration.annotation.Poller;
import org.springframework.integration.channel.DirectChannel;
import org.springframework.integration.config.EnableIntegration;
import org.springframework.integration.core.MessageSource;
import org.springframework.integration.dsl.IntegrationFlow;
import org.springframework.integration.file.FileHeaders;
import org.springframework.integration.file.FileReadingMessageSource;
import org.springframework.integration.file.filters.CompositeFileListFilter;
import org.springframework.integration.file.filters.LastModifiedFileListFilter;
import org.springframework.integration.file.filters.SimplePatternFileListFilter;
import org.springframework.integration.file.transformer.FileToStringTransformer;
import org.springframework.integration.handler.advice.AbstractRequestHandlerAdvice;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Gestionnaire d'intégration des données.
 * @author Constantin Drabo
 * @version 1.0
 * @since 12/01/2024
 */
@EnableIntegration
@Configuration
public class IntegrationFichier {

    //Repertoire source des fichiers
    @Value("${repertoire.source}")
    private String repertoireSource;
    //Repertoire de destination
    @Value("${repertoire.destination}")
    private String repertoireDestination;
    //Rechercher les fichiers avec l'extension .XML
    private static final String FILEPATTERN = "*.xml";
    //Injecte le service d'enregistrement d'une personne.
    @Autowired
    private PersonneMessageToDatabase personneMessageToDatabase;

    /**
     * Canal de transmission de la source vers la destination.
     * @return MessageChannel
     */
    @Bean
    public MessageChannel fileInputChannel() {
        return new DirectChannel();
    }

    /**
     * Méthode de lecture du fichier source.
     * @return MessageSource<File>
     */
    @Bean
    @InboundChannelAdapter(value = "fileInputChannel", poller = @Poller(fixedDelay = "1000"))
        public MessageSource<File> fileReadingMessageSource() {
        CompositeFileListFilter<File> filters = new CompositeFileListFilter<>();
        filters.addFilter(new SimplePatternFileListFilter(FILEPATTERN));
        filters.addFilter(new LastModifiedFileListFilter());

        FileReadingMessageSource sourceReader = new FileReadingMessageSource();
        sourceReader.setAutoCreateDirectory(true);
        sourceReader.setDirectory(new File(repertoireSource));
        sourceReader.setFilter(filters);
        return sourceReader;
    }

    @Bean
    public AbstractRequestHandlerAdvice advice() {
        return new AbstractRequestHandlerAdvice() {
            @Override
            protected Object doInvoke(final AbstractRequestHandlerAdvice.ExecutionCallback callback, final Object target, final Message<?> message) {
                try {
                    File file = message.getHeaders().get(FileHeaders.ORIGINAL_FILE, File.class);
                    fileProcessing(file);
                    try {
                        Object result = callback.execute();
                        if (file != null) {
                            boolean moved = file.renameTo(new File(repertoireDestination, file.getName()));
                            if (moved) {
                                System.out.println("Fichier déplacé vers " + repertoireDestination);
                            }
                        }
                        return result;
                    } catch (Exception e) {
                        throw e;
                    }

                } catch (JAXBException | ParseException ex) {
                    Logger.getLogger(IntegrationFichier.class.getName()).log(Level.SEVERE, null, ex);
                }
                return null;
            }
        };
    }

    /**
     * Traitement du contenu du fichier XML source.
     * @param source Fichier source
     * @return PersonneMessage
     * @throws JAXBException
     * @throws ParseException
     */
    public PersonneMessage fileProcessing(final File source) throws JAXBException, ParseException {
        JAXBContext jaxbContext = JAXBContext.newInstance(PersonneMessage.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        PersonneMessage personne = (PersonneMessage) jaxbUnmarshaller.unmarshal(source);
        return personne;
    }

    @Bean
    public FileToStringTransformer fileToStringTransformer() {
        return new FileToStringTransformer();
    }

    /**
     * Workflow de l'ingestion du fichier source.
     * @return IntegrationFlow
     */
    @Bean
    public IntegrationFlow processFileFlow() {
        return IntegrationFlow
                .from("fileInputChannel")
                .transform(fileToStringTransformer())
                .handle("fileProcessorToPersonne", "process",
                        e ->  e.advice(advice())).get();
    }

    /**
     * Transformation du fichier XML en objet de type PersonneMessage
     * @return FileProcessorToPersonne
     */
    @Bean(name = {"fileProcessorToPersonne"})
    public FileProcessorToPersonne fileProcessor() {
        return new FileProcessorToPersonne();
    }

    /**
     * Transformation du fichier XML en objet de type PersonneMessage
     * @return FileProcessorToPersonne
     */
    @Bean(name = {"fileProcessorToDatabase"})
    public PersonneMessageToDatabase fileProcessorDatabase() {
        return new PersonneMessageToDatabase();
    }
}
