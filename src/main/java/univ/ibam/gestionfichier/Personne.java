package univ.ibam.gestionfichier;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Constantin Drabo
 * @version 1.0
 * @since 12/01/2024
 */
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Personne {
    @Id
    private String identifiant;
    private String nom;
    private String prenom;
    private int age;
}
