/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package univ.ibam.gestionfichier;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;


/**
 * Modèle pour stocker les informations d'une personne.
 * @author Constantin Drabo
 */


@Getter
@Setter
@ToString
@AllArgsConstructor
@NoArgsConstructor
@XmlRootElement(name = "Personne")
@XmlAccessorType(XmlAccessType.FIELD)
public class PersonneMessage {
    @XmlElement(name = "identifiant")
    private String identifiant;
    @XmlElement(name = "nom")
    private String nom;
    @XmlElement(name = "prenom")
    private String prenom;
    @XmlElement(name = "age")
    private int age;
}
