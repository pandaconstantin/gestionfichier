package univ.ibam.gestionfichier;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service pour l'enregistrement d'une personne
 * @author Constantin Drabo
 * @version 1.0
 * @since 12/01/2024
 */
@Component
public class PersonneMessageToDatabase {
    //Injection du service contenant les interfaces pour la gestion d'une personne.
    @Autowired
    private  PersonneRepository personneRepository;

    /**
     * Enregistrer une personne.
     * @param personneMessage Message contenant les informations d'une personne.
     */
    @Transactional
    public void enregistrerPersonne(final PersonneMessage personneMessage) {
       Personne personne = new Personne();
       personne.setIdentifiant(personneMessage.getIdentifiant());
       personne.setNom(personneMessage.getNom());
       personne.setPrenom(personneMessage.getPrenom());
       personne.setAge(personneMessage.getAge());
       personneRepository.save(personne);
    }
}
