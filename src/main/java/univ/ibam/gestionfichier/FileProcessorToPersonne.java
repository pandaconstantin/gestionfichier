package univ.ibam.gestionfichier;

import org.springframework.messaging.Message;

/**
 * @author Constantin Drabo
 * @version 1.0
 * @since 12/01/2024
 */
public class FileProcessorToPersonne {

    private static final String HEADERFILENAME = "file_name";
    private static final String MSG = "%s received. Content: %s";

    /**
     * Méthode pour la gestion du fichier.
     *
     * @param msg
     */
    public void process(final Message<String> msg) {
        String fileName = (String) msg.getHeaders().get(HEADERFILENAME);
        String content = msg.getPayload();
    }
}
